# Proyecto TechU - Practitioner

Proyecto de culminación de Curso - Nivel Practitioner. 

El siguiente parte de la plantilla de proyecto `Polymer 3 - Starter Kit` como partida inicial.

Las principales funcionalidads que se presentan en el proyecto son:

* **Login** Validación de Ingreso al aplicativo
* **Listar Cuentas** Lista de las cuentas del cliente
* **Listar Movimientos** Listar las transacciones realizadas por cada cuenta
* **Agregar Movimientos** Agregar 
* **Actualizar Perfil** Actualización de datos del perfil

### Principales WebComponents

| Componentes | Descripción
|--|--
| [paper-input-elements](https://www.webcomponents.org/collection/PolymerElements/paper-input-elements) | Componentes para Ingreso de datos
| [paper-ui-elements](https://www.webcomponents.org/collection/PolymerElements/paper-input-elements) | Componentes para el control acciones de botones y listas
| [iron-elements](https://www.webcomponents.org/collection/PolymerElements/iron-elements) | Componentes para la comunicación de peticiones hacia el servidor

### Configuración

##### Prerequisites

Instalar [Polymer CLI](https://github.com/Polymer/polymer-cli) usando
[npm](https://www.npmjs.com) (Asumimos la pre-instalación de [node.js](https://nodejs.org)).

    npm install -g polymer-cli

##### Inicializar proyecto

    cd front-techu-bbva
    npm init

### Iniciar servidor de desarrollo

El proyecto inicaliza en la siguiente ruta `http://127.0.0.1:8081`:

    npm start