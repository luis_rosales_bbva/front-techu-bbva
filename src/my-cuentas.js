import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-list/iron-list.js';
import '@polymer/app-route/app-location.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/paper-item/paper-item.js';
import '@polymer/paper-item/paper-item-body.js';
import '@polymer/paper-item/paper-icon-item.js';

import './shared-styles.js';
import Local from './storage/local';


class MyCuentas extends PolymerElement {
  constructor(){
    console.info("const");
    super();
  }

  static get properties() {
    return {
      _mostrarMovimientos: {
        type: Function
      }
    };
  }

  ready(){
    super.ready();
    let local = new Local();

    console.info('ingreso');

    let usuario = local.obtenerUsuario();
    let token = local.obtenerToken();

    this.$.AjaxPost.url="http://localhost:3000/api/accounts/" + usuario.id;
    this.$.AjaxPost.headers['authorization'] = token;
    this.$.AjaxPost.generateRequest();

  }

  attached() {
    console.log('attached!');
  }

  static get template() {
    return html`
    <style include="shared-styles">
      :host {
        display: block;
        padding: 10px;
        height: 100vh;
      }
      .cont{
        background: white;
        padding: 20px;
        height: 100vh;
      }
      iron-list {
        flex: 1;
        height: 100h;
      }
      .item-card:hover {
        background-color: #f8cd51;
      }
      .indigo{
        width: 100%;
      }
      .item-mov {
        padding: 0px 5px!important;
      }
      h3 {
        margin: 10px;
      }
      paper-item.fancy {
        --paper-item-focused: {
          background: var(--paper-amber-500);
          font-weight: bold;
        };
        --paper-item-focused-before: {
          opacity: 0;
        };
      }
      .avatar {
        display: inline-block;
        box-sizing: border-box;
        width: 40px;
        height: 40px;
        border-radius: 50%;
        background: var(--paper-amber-500);
      }
    </style>
    <app-location route="{{route}}"></app-location>

    <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">
    </app-route>

    <iron-ajax id="AjaxPost" 
      last-response="{{data}}" method="GET" content-type="application/json" 
      handle-as="json"></iron-ajax>

    <my-app>
    <div class="cont">
        <h3>Mis Cuentas</h3>
        <div role="listbox">
          <dom-repeat items="{{data.accounts}}">
            <template is="dom-repeat" items="[[items]]" as="someItem">
              <paper-icon-item on-tap="_mostrarMovimientos" class="item-mov">
                <div class="avatar" slot="item-icon">
                  <template is="dom-if" if="[[_isPen(item.currency)]]" restamp>
                    <img src="/assets/PEN.svg" alt="PEN"></img>
                  </template>
                  <template is="dom-if" if="[[!_isPen(item.currency)]]" restamp>
                    <img src="/assets/USD.svg" alt="USD"></img>
                  </template>
                </div>
                <paper-item-body two-line>
                  <div>[[_formatCuenta(item.accountNumber)]]</div>
                  <div secondary>{{item.type}}</div>
                </paper-item-body>
                
                </paper-icon-button>
              </paper-icon-item>
            </template>
          </dom-repeat>
        </div>
    </div>
    
    </my-app>
    `;
  }

  _formatCuenta(value){
    return value;
  }

  _isPen(value){
    return value == 'PEN';
  }

  _mostrarMovimientos(nroCuenta){
    this.set('route.path', 'movimientos/' + nroCuenta.model.item._id);
    sessionStorage.setItem("_id", nroCuenta.model.item._id);
    
  }
}

window.customElements.define('my-cuentas', MyCuentas);
