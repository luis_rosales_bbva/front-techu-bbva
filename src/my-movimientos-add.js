import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-card';
import '@polymer/paper-dialog/paper-dialog.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-radio-button/paper-radio-button.js';
import '@polymer/paper-radio-group/paper-radio-group.js';
import './shared-styles.js';
import Local from './storage/local';

class MyMovimientosAdd extends PolymerElement {
  constructor(){
    super();
  }

  static get properties() {
    return {
      _openDialog: {
        type: Function
      },
      _agregarMovimiento: {
        type: Function
      },
      _addMovimiento: {
        type: Function
      },
    };
  }

  ready(){
    super.ready();
    let local = new Local();
    let token = local.obtenerToken();
    this.$.AjaxPost.headers['authorization'] = token;   
  }

  _agregarMovimiento(){
    console.info("agregar");
    let mov = new Object();
    mov.account = sessionStorage.getItem("_id");
    mov.amount = this.$.amount.value;
    mov.currency = this.$.currency.selected;
    mov.operation = this.$.operation.value;

    console.info(mov);

    this.$.AjaxPost.body = mov;
    this.$.AjaxPost.generateRequest();
  }

  handleResponse(data){
    this.set('route.path', 'movimientos');
  }

  _addMovimiento(){
    this.$.dialog.open();
  }

  static get template() {
    return html`
    <style include="shared-styles">
      :host {
        display: block;
        padding: 10px;
      }
      .cont{
        background: white;
        padding: 20px;
        height: 100vh;
      }
      .item-card:hover {
        background-color: #f8cd51;
      }
      .indigo{
        width: 100%;
      }
      paper-fab {
        position: fixed;
        right: 30px;
        bottom: 15px;
        background: #004481;
      }
      .center{
        text-align: center;
      }
      paper-button.indigo {
        width: 80%;
        margin: 20px 10px 10px 0px;
        background-color: var(--app-primary-color);
        color: white;
        --paper-button-raised-keyboard-focus: {
          background-color: var(--app-primary-color) !important;
          color: white !important;
        };
      }
    </style>
    <app-location route="{{route}}"></app-location>

    <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">
    </app-route>

    <iron-ajax id="AjaxPost" url="http://localhost:3000/api/accounts/transactions/" 
      on-response="handleResponse"
      last-response="{{data}}" method="POST" content-type="application/json" handle-as="json">
    </iron-ajax>

    <my-app>
    <div class="cont">
      <h3>Realizar Movimiento</h3>
      <paper-input always-float-label id="_id" value="{{data.user._id}}" hidden></paper-input>
      <paper-input always-float-label id="operation" label="Nro Operación"
        value="{{_nroOperacion()}}" disabled></paper-input>
      <label id="labelS">Moneda:</label>
      <paper-radio-group id="currency" aria-labelledby="labelS" selected="PEN">
        <paper-radio-button name="PEN">S/.</paper-radio-button>
        <paper-radio-button name="USD">US$</paper-radio-button>
      </paper-radio-group>
      <paper-input always-float-label id="amount" label="Importe" type="number"
        value="0.00"></paper-input>
      <div class="center">
        <paper-button raised class="custom indigo" on-tap="_addMovimiento">Aceptar</paper-button>  
      </div>
    </div>
    
    </my-app>
    <paper-dialog id="dialog" modal>
      <h3>¿Desea continuar?</h3>
      <div class="buttons">
        <paper-button dialog-confirm on-tap="_agregarMovimiento">Si</paper-button>
        <paper-button dialog-dismiss>Cancelar</paper-button>
      </div>
    </paper-dialog>
    `;
  }

  _nroOperacion(){
    let v1 = Math.floor(Math.random() * (999 - 100) ) + 100;
    let v2 = Math.floor(Math.random() * (99999 - 10000) ) + 10000;
    return "OP-F4E44" + v2+"-" + v1;
  }

}

window.customElements.define('my-movimientos-add', MyMovimientosAdd);
