import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/app-route/app-location.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/paper-dialog/paper-dialog.js';
import '@polymer/iron-image/iron-image.js';
import '@polymer/paper-input/paper-input.js';

import '@polymer/paper-radio-button/paper-radio-button.js';
import '@polymer/paper-radio-group/paper-radio-group.js';
import '@polymer/paper-spinner/paper-spinner.js';


import './shared-styles.js';
import Local from './storage/local';


class MyPerfil extends PolymerElement {
  constructor(){
    super();
  }

  static get properties() {
    return {
      name: String,
      lastname: String,
      photo: String,
      _actualizarPerfil: {
        type: Function
      },
      _actualizarImmagen: {
        type: Function
      }
    };
  }

  ready(){
    super.ready();
    let local = new Local();

    console.info('ingreso');

    let usuario = local.obtenerUsuario();
    let token = local.obtenerToken();

    this.$.AjaxPost.body = usuario;
    this.$.AjaxPost.headers['authorization'] = token;
    this.$.AjaxPost.generateRequest();

    this.set('name', usuario.name);
    this.set('lastname', usuario.lastname);
    this.set('photo', usuario.photo);
    
  }

  static get template() {
    return html`
    <style include="shared-styles">
      :host {
        display: block;
        padding: 10px;
      }
      iron-list {
        flex: 1;
        height: 100h;
      }
      .item-card:hover {
        background-color: #f8cd51;
      }
      .indigo{
        width: 100%;
      }
      #example-sizing-contain {
        width: 100px;
        height: 100px;
        background: #ddd;
        border-radius: 50%;
      }
      .center{
        text-align: center;
      }
      .content{
        background: white;
        padding: 20px;
        height: 100vh;
      }
      paper-button.indigo {
        width: 80%;
        margin: 20px 10px 10px 0px;
        background-color: var(--app-primary-color);
        color: white;
        --paper-button-raised-keyboard-focus: {
          background-color: var(--app-primary-color) !important;
          color: white !important;
        };
      }
    </style>
    
    <app-location route="{{route}}"></app-location>
    <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">
    </app-route>

    <iron-ajax id="AjaxPost" 
      url="http://localhost:3000/api/users/prolife" 
      last-response="{{data}}" 
      method="GET" 
      content-type="application/json" 
      handle-as="json">
    </iron-ajax>

    <iron-ajax id="updateUser" 
      url="http://localhost:3000/api/users/" 
      last-response="{{dataUpdate}}" 
      method="PUT" 
      on-response="handleResponseUpdateUser"
      content-type="application/json" 
      handle-as="json">
    </iron-ajax>

    <my-app>
    <div class="content">
    <h1>Mi Perfil</h1>
    <div class="center">
      <iron-image on-tap="_actualizarImmagen" sizing="contain" id="example-sizing-contain" alt="The Polymer logo." src="[[photo]]"></iron-image>
    </div>
    <paper-input always-float-label id="_id" label="Documento" value="{{data.user._id}}" hidden></paper-input>
    <paper-input always-float-label id="identity" label="Documento" 
      required auto-validate error-message="Ingresar Documento"
      value="{{data.user.identity}}"></paper-input>
    <paper-input always-float-label id="name" label="Nombre" 
      required auto-validate error-message="Ingresar Nombre"
      value="[[name]]"></paper-input>
    <paper-input always-float-label id="lastname" label="Apellido" 
      required auto-validate error-message="Ingresar Apellido"
      value="[[lastname]]"></paper-input>
    <paper-input always-float-label id="email" label="Correo" value="{{data.user.email}}" disabled></paper-input>

    <label id="labelS">Sexo:</label>
    <paper-radio-group id="gender" aria-labelledby="labelS" selected="{{data.user.gender}}">
      <paper-radio-button name="M">Masculino</paper-radio-button>
      <paper-radio-button name="F">Femenino</paper-radio-button>
    </paper-radio-group>
    
    <div class="center">
      <paper-button raised class="custom indigo" on-tap="_actualizarPerfil">Actualizar</paper-button>  
    </div>

    </div>
    
    </my-app>
    `;
  }

  _actualizarImmagen(){
    console.info("actua");
  }

  _actualizarPerfil(){
    if(this.$.identity.value == null || 
      this.$.name.value == null|| 
      this.$.lastname.value == null){
      return;
    }

    let usuario = new Object();
    usuario._id = this.$._id.value;
    usuario.identity = this.$.identity.value;
    usuario.name = this.$.name.value;
    usuario.lastname = this.$.lastname.value;
    usuario.gender = this.$.gender.selected;

    let local = new Local();

    let token = local.obtenerToken();

    this.$.updateUser.body = usuario;
    this.$.updateUser.headers['authorization'] = token;
    this.$.updateUser.generateRequest();
  }

  handleResponseUpdateUser(data){
    console.log("data");
    console.log();
    let response = data.detail.__data.xhr.response;
    let local = new Local();
    
    if (response.success){
      console.info("HOLA");
    }else{
      alert("Error response"); 
    }
  }


}

window.customElements.define('my-perfil', MyPerfil);