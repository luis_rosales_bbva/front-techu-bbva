import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import { setPassiveTouchGestures, setRootPath } from '@polymer/polymer/lib/utils/settings.js';
import '@polymer/app-layout/app-drawer/app-drawer.js';
import '@polymer/app-layout/app-drawer-layout/app-drawer-layout.js';
import '@polymer/app-layout/app-header/app-header.js';
import '@polymer/app-layout/app-header-layout/app-header-layout.js';
import '@polymer/app-layout/app-scroll-effects/app-scroll-effects.js';
import '@polymer/app-layout/app-toolbar/app-toolbar.js';
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/iron-selector/iron-selector.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import './my-icons.js';

// Gesture events like tap and track generated from touch will not be
// preventable, allowing for better scrolling performance.
setPassiveTouchGestures(true);

// Set Polymer's root path to the same value we passed to our service worker
// in `index.html`.
setRootPath(MyAppGlobals.rootPath);

class MyAppNav extends PolymerElement {
  static get template() {
    return html`
      <app-location route="{{route}}" url-space-regex="^[[rootPath]]">
      </app-location>

      <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">
      </app-route>

      <iron-pages selected="[[page]]" attr-for-selected="name" role="main">
          <my-login  name="login"></my-login>
          <my-perfil  name="perfil"></my-perfil>
          <my-cuentas name="cuentas"></my-cuentas>
          <my-movimientos name="movimientos"></my-movimientos>
          <my-movimientos-add name="movimientosAdd"></my-view3>
          <my-view404 name="view404"></my-view404>
      </iron-pages>
    `;
  }

  static get properties() {
    return {
      page: {
        type: String,
        reflectToAttribute: true,
        observer: '_pageChanged'
      },
      routeData: Object,
      subroute: Object
    };
  }

  static get observers() {
    return [
      '_routePageChanged(routeData.page)'
    ];
  }

  _routePageChanged(page) {
     // Show the corresponding page according to the route.
     //
     // If no page was found in the route data, page will be an empty string.
     // Show 'view1' in that case. And if the page doesn't exist, show 'view404'.
    if (!page) {
      this.page = 'login';
    } else if (['login', 'perfil','cuentas', 'movimientos', 'movimientosAdd','view3'].indexOf(page) !== -1) {
      this.page = page;
    } else {
      this.page = 'view404';
    }
    // Close a non-persistent drawer when the page & route are changed.
    /*if (!this.$.drawer.persistent) {
      this.$.drawer.close();
    }
    */
   
  }

  _pageChanged(page) {
    // Import the page component on demand.
    //
    // Note: `polymer build` doesn't like string concatenation in the import
    // statement, so break it up.
    switch (page) {
      case 'login':
        import('./my-login.js');
        break;
      case 'perfil':
        import('./my-perfil.js');
        break;
      case 'cuentas':
        import('./my-cuentas.js');
        break;
      case 'movimientos':
        import('./my-movimientos.js');
        break;
      case 'movimientosAdd':
        import('./my-movimientos-add.js');
        break;
      case 'view404':
        import('./my-view404.js');
        break;
    }
    
  }
}

window.customElements.define('my-appnav', MyAppNav);
