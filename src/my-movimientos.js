import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-card';
import '@polymer/paper-dialog/paper-dialog.js';
import '@polymer/paper-item/paper-item.js';
import '@polymer/paper-item/paper-item-body.js';
import '@polymer/paper-item/paper-icon-item.js';
import '@polymer/app-route/app-location.js';
import '@polymer/iron-pages/iron-pages.js';
import './shared-styles.js';
import Local from './storage/local';

class MyMovimientos extends PolymerElement {
  constructor(){
    super();
  }

  static get properties() {
    return {
      _openDialog: {
        type: Function
      },
      _agregarMovimiento: {
        type: Function
      }
    };
  }

  ready(){
    super.ready();
    let local = new Local();

    console.info("hola");

    let token = local.obtenerToken();

    let cuenta = new Object();
    cuenta.id = sessionStorage.getItem("_id");

    this.$.AjaxPost.url = 'http://localhost:3000/api/accounts/transactions/' + cuenta.id;
    this.$.AjaxPost.body = cuenta;
    this.$.AjaxPost.headers['authorization'] = token;
    this.$.AjaxPost.generateRequest();
  }

  _agregarMovimiento(){
    this.set('route.path', 'movimientosAdd');
  }

  _openDialog(){
    this.set('route.path', 'movimientosAdd');
  }

  static get template() {
    return html`
    <style include="shared-styles">
      :host {
        display: block;
        padding: 10px;
      }
      .cont{
        background: white;
        padding: 20px;
        height: 100vh;
      }
      paper-fab {
        position: fixed;
        right: 15px;
        bottom: 15px;
        background: #004481;
      }
      .avatar {
        display: inline-block;
        box-sizing: border-box;
        width: 40px;
        height: 40px;
        border-radius: 50%;
        background: var(--paper-amber-500);
      }
      .item-mov {
        padding: 0px 5px!important;
      }
      h3 {
        margin: 10px;
      }
    </style>

    <app-location route="{{route}}"></app-location>

    <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">
    </app-route>

    <iron-ajax id="AjaxPost"
      last-response="{{data}}" 
      method="GET" content-type="application/json" 
      handle-as="json"></iron-ajax>
    <my-app>

    <div class="cont">
      <h3>Mis Movimientos</h3>
      <div role="listbox">
        <dom-repeat id="list" items="{{data.transactions}}">
          <template is="dom-repeat" items="[[items]]" as="someItem">
            <paper-icon-item class="item-mov">
              <div class="avatar" slot="item-icon">
                <template is="dom-if" if="[[_isPen(item.currency)]]" restamp>
                  <img src="/assets/PEN.svg" alt="PEN"></img>
                </template>
                <template is="dom-if" if="[[!_isPen(item.currency)]]" restamp>
                  <img src="/assets/USD.svg" alt="USD"></img>
                </template>
              </div>
              <paper-item-body two-line>
                <div>{{item.operation}}</div>
                <div secondary>{{item.currency}}</div>
              </paper-item-body>
              <div>[[_formatMonto(item.amount, item.currency)]]</div>
            </paper-icon-item>
          </template>
        </dom-repeat>
      </div>
    </div>
    
    </my-app>
    <paper-fab icon="add" on-tap="_agregarMovimiento"></paper-fab>

    <paper-dialog id="dialog" modal>
      <h3>text</h3>
      <div class="buttons">
        <paper-button dialog-confirm on-tap="_agregarMovimiento">Close</paper-button>
      </div>
    </paper-dialog>
    `;
  }

  _formatMonto(value, currency){
    if(currency == 'PEN'){
      return 'S/  ' + (Math.round(value * 100) / 100).toFixed(2);
    }else{
      return 'US$ ' + (Math.round(value * 100) / 100).toFixed(2);
    }
  }

  _isPen(value){
    return value == 'PEN';
  }

}

window.customElements.define('my-movimientos', MyMovimientos);
