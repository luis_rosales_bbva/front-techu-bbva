import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import { setPassiveTouchGestures, setRootPath } from '@polymer/polymer/lib/utils/settings.js';
import '@polymer/app-layout/app-drawer/app-drawer.js';
import '@polymer/app-layout/app-drawer-layout/app-drawer-layout.js';
import '@polymer/app-layout/app-header/app-header.js';
import '@polymer/app-layout/app-header-layout/app-header-layout.js';
import '@polymer/app-layout/app-scroll-effects/app-scroll-effects.js';
import '@polymer/app-layout/app-toolbar/app-toolbar.js';
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/iron-selector/iron-selector.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/paper-dialog/paper-dialog.js';
import '@polymer/iron-image/iron-image.js';

import './my-icons.js';
import Local from './storage/local';

// Gesture events like tap and track generated from touch will not be
// preventable, allowing for better scrolling performance.
setPassiveTouchGestures(true);

// Set Polymer's root path to the same value we passed to our service worker
// in `index.html`.
setRootPath(MyAppGlobals.rootPath);

class MyApp extends PolymerElement {
  constructor(){
    super();
  }

  ready(){
    super.ready();
    const local = new Local();
    let usuario = local.obtenerUsuario(); 
    this.set('name', usuario.name + ' ' + usuario.lastname);
    this.set('photo', usuario.photo);
  }

  static get properties() {
    return {
      name: String,
      _openDialog: {
        type: Function
      },
      _salir: {
        type: Function
      }
    };
  }

  _openDialog(){
    this.$.salirDialog.open();
  }
  
  _salir(){
    console.info("sal");
    const local = new Local();
    local.limpiar();
    this.set('route.path', 'login');
    location.reload();
  }
  
  static get template() {
    return html`
      <style>
        :host {
          --app-primary-color: #004481;
          --app-secondary-color: black;

          display: block;
        }

        app-drawer-layout:not([narrow]) [drawer-toggle] {
          display: none;
        }
        
        #contentContainer{
          background: #0044810d;
        }

        app-header {
          color: #fff;
          background-color: var(--app-primary-color);
        }

        app-header paper-icon-button {
          --paper-icon-button-ink-color: white;
        }

        .drawer-list {
          margin: 0 20px;
        }

        .drawer-list a {
          display: block;
          padding: 0 16px;
          text-decoration: none;
          color: var(--app-secondary-color);
          line-height: 40px;
        }

        .drawer-list a.iron-selected {
          color: black;
          font-weight: bold;
        }
        #example-sizing-contain {
          width: 40px;
          height: 40px;
          background: #ddd;
          border-radius: 50%;
        }
        .nombre{
          margin: 12px;
        }
      </style>
      <app-location route="{{route}}"></app-location>

      <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">
      </app-route>

      <app-drawer-layout fullbleed="" narrow="{{narrow}}" class="draw">
        <!-- Drawer content -->
        <app-drawer id="drawer" slot="drawer" swipe-open="[[narrow]]" class="draw">
          <app-toolbar>
            <iron-image sizing="contain" id="example-sizing-contain" alt="The Polymer logo." src="[[photo]]"></iron-image>
            <span class="nombre">[[name]]</span>
          </app-toolbar>
          <iron-selector selected="[[page]]" attr-for-selected="name" class="drawer-list" role="navigation">
            <a name="cuentas" href="[[rootPath]]cuentas"><iron-icon icon="view-list"></iron-icon>Cuentas</a>
            <a name="perfil" href="[[rootPath]]perfil"><iron-icon icon="account-box"></iron-icon>Perfil</a>
          </iron-selector>
        </app-drawer>

        <!-- Main content -->
        <app-header-layout has-scrolling-region="">

          <app-header slot="header" condenses="" reveals="" effects="waterfall">
            <app-toolbar>
              <paper-icon-button icon="my-icons:menu" drawer-toggle=""></paper-icon-button>
              <div main-title="">TechU - BBVA Practitioner</div>
              <paper-icon-button icon="power-settings-new" on-tap="_openDialog"></paper-icon-button>
            </app-toolbar>
          </app-header>
          <slot></slot>
        </app-header-layout>
      </app-drawer-layout>

      <paper-dialog id="salirDialog" modal>
        <h3>¿Desea Salir?</h3>
        <div class="buttons">
          <paper-button dialog-confirm on-tap="_salir">Si</paper-button>
          <paper-button dialog-dismiss>Cancelar</paper-button>
        </div>
      </paper-dialog>
    `;
  }
}

window.customElements.define('my-app', MyApp);
