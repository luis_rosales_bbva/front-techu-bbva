export default class Local {
    constructor() {
    }

    guardarSesion(datos){
        localStorage.setItem('token', datos.token);
        localStorage.setItem('user', JSON.stringify(datos.user));
    }

    obtenerUsuario(){
        return JSON.parse(localStorage.getItem('user'));
    }

    obtenerToken(){
        return localStorage.getItem('token');
    }

    limpiar(){
        localStorage.clear();
    }
}