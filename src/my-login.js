import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import { setPassiveTouchGestures, setRootPath } from '@polymer/polymer/lib/utils/settings.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-card/paper-card.js';
import '@polymer/app-route/app-route.js';
import '@polymer/app-route/app-location.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/iron-ajax/iron-ajax.js';

import './shared-styles.js';
import Local from './storage/local';


setPassiveTouchGestures(true);
setRootPath(MyAppGlobals.rootPath);

class MyLogin extends PolymerElement {
  constructor() {
    super();
  }
  static get properties() {
    return {
      _login: {
        type: Function
      },
      _irPrincipal: {
        type: Function
      }
    }
  }
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;
          padding: 10px;
          background: rgb(0,68,129);
          background: linear-gradient(115deg, rgba(0,68,129,1) 50%, rgba(7,33,70,1) 50%, rgba(131,58,180,1) 004481%);
          height: 100vh;
        }
        .card {
          margin-top: 100px!important;
          margin: auto;
          background-color: rgba(255, 255, 255, .8);
          max-width: 500px;
        }
        .header {
          text-align: center;
        }
        .btnLogin {
          text-align: center;
        }
        .logo{
          width: 80%;
          max-width: 500px;
        }
        .center{
          text-align: center;
          margin-top: 80px;
        }
        paper-button.indigo {
          width: 80%;
          margin: 20px 10px 10px 0px;
          background-color: #004481;
          color: white;
          --paper-button-raised-keyboard-focus: {
            background-color: #004481 !important;
            color: white !important;
          };
        }
      </style>

      <app-location route="{{route}}"></app-location>

      <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">
      </app-route>

      <iron-ajax id="AjaxPost" url="http://localhost:3000/api/users/authenticate" method="POST" content-type="application/json" handle-as="json" on-response="handleResponse" on-error="handleAjaxPostError"></iron-ajax>

      <div class="center">
        <img src="/assets/logo.png" alt="TechU" class="logo"></img>
      </div>
      <div class="card">
        <div class="header">
          
        </div>
        <paper-input always-float-label label="Usuario" name="nombre" id="nombre" value="jromero@gmail.com"></paper-input>
        <paper-input always-float-label label="Contraseña" name="clave" id="clave" value="123"></paper-input>
        <br>
        <div class="btnLogin">
          <paper-button raised class="custom indigo" on-tap="_login">Ingresar</paper-button>
        </div>
      </div>
    `;
  }

  _irPrincipal() {
    this.set('route.path', 'cuentas');
  }

  handleResponse(data){
    console.log("data");
    console.log();
    let response = data.detail.__data.xhr.response;
    let local = new Local();
    
    if (response.success){
      local.guardarSesion(response);
      this.set('route.path', 'cuentas');
    }else{
      alert("Error response"); 
    }
  }
  
  handleAjaxPostError(event, request){
    alert("Usuario y/o contraseña incorrecto");
  }

  _login() {
    
    let usuario = new Object();
    usuario.email = this.$.nombre.value;
    usuario.password = this.$.clave.value;

    this.$.AjaxPost.body = usuario;
    this.$.AjaxPost.generateRequest();
  }

}

window.customElements.define('my-login', MyLogin);
